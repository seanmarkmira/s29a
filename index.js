const express = require('express');
const app = express();

let users = [
];

app.use(express.json());
//we use this to accept from any sources
app.use(express.urlencoded({extended:true}));

const port = 3000;

app.listen(port, ()=>{
	console.log(`Server is running at port ${port}`)
});

app.get('/home',(req,res)=>{
	res.send('This is home')
})

app.get('/users',(req,res)=>{
	res.send(users)
})

app.post('/users',(req,res)=>{
	if(req.body.firstname && req.body.lastname && req.body.email){
		console.log('complete entry')
		users.push(req.body);
		console.log(users)
		res.send(`firstname:${req.body.firstname}, lastname:${req.body.lastname}, email:${req.body.email} successfully registered`);
	}else{
		console.log('there are missing inputs');
	}
})
